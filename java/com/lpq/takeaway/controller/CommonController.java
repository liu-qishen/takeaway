package com.lpq.takeaway.controller;

import com.lpq.takeaway.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-01 09:36
 **/
@SuppressWarnings({"all"})
@Slf4j
@RestController
@RequestMapping("/common")
public class CommonController {
    @Value("${takeaway.path}") //获取yml文件的值注入
    private String basePath;
    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file){
        //file是一个临时文件 需要转存到指定位置 否则本次请求完成后临时文件会被删除
        log.info("文件上传，上传的文件为：{}",file.toString());
        //获取上传文件的原始文件名 不建议使用 因为可能重复
        String originalFilename = file.getOriginalFilename(); //file.jpg
        //获得文件后缀，用最后一个.分割
        String hz = originalFilename.substring(originalFilename.lastIndexOf("."));//jpg
        //生成uuid 来当新文件名
        String newFileName = UUID.randomUUID().toString() + hz;
        //创建一个目标目录对象
        File dir = new File(basePath);
        //如果当前目标目录不存在 则创建
        if (!dir.exists()){
            dir.mkdirs();
        }
        try {
            //将临时文件存到指定位置
            file.transferTo(new File(basePath + newFileName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return R.success(newFileName);
    }

    /**
     * 文件下载
     * @param name
     * @param response
     */
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response){
        try {
            //输入流读取文件内容
            FileInputStream inputStream = new FileInputStream(new File(basePath + name));
            //输出流将文件写回浏览器 在浏览器展示图片
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg"); //设置文件类型
            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = inputStream.read(bytes)) != -1){ //读写文件
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            inputStream.close();
            outputStream.close(); //关流
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
