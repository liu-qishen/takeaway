package com.lpq.takeaway.controller;

import com.lpq.takeaway.service.OrderDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-07-06 16:56
 **/
@Slf4j
@SuppressWarnings({"all"})
@RestController
@RequestMapping("/orderDetail")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;
}
