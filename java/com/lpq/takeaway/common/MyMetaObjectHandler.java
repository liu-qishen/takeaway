package com.lpq.takeaway.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 用于自动填充公共字段
 * @author: PQ丶Lee
 * @createTime: 2023-06-30 09:21
 **/
@Component //交给spring管理
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {
    /**
     * 新增操作 自动填充 当指定字段进行插入操作时执行
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("公共字段自动填充【insert】->{}",metaObject.toString());
        //第一个参数是指定的字段 第二个参数是要设置的值
        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("createUser",BaseContext.getCurrentId());
        metaObject.setValue("updateUser",BaseContext.getCurrentId());
    }

    /**
     * 修改操作 自动填充 当指定字段进行修改操作时执行
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段自动填充【update】->{}",metaObject.toString());
        metaObject.setValue("updateTime",LocalDateTime.now());
        metaObject.setValue("updateUser",BaseContext.getCurrentId());
    }
}
