package com.lpq.takeaway;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @author: PQ丶Lee
 * @createTime: 2023-06-28 09:55
 **/
@Slf4j
@SpringBootApplication
@ServletComponentScan //识别拦截器等
@EnableCaching //开启缓存
public class TakeawayApplication {
    public static void main(String[] args) {
        SpringApplication.run(TakeawayApplication.class,args);
        log.info("项目启动成功");
    }
}
